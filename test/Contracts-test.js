const { expect } = require("chai");
const Contracts = require("../src/Contracts");
const msgpack = require("@msgpack/msgpack");

describe("Contracts", () => {
    describe("toVaruint", () => {
        context("given 200000", () => {
            const result = Contracts.toVaruint(200000);

            it("returns the correct value", () => {
                expect(result).to.deep.equal([192, 154, 12]);
            });
        });

        context("given 1000", () => {
            const result = Contracts.toVaruint(1000);

            it("returns the correct value", () => {
                expect(result).to.deep.equal([232, 7]);
            });
        });

        context("given 18446744073709551615", () => {
            const result = Contracts.toVaruint(18446744073709551615);

            it("returns the correct value", () => {
                expect(result).to.deep.equal([128, 128, 128, 128, 128, 128, 128, 128, 128, 2]);
            });
        });
    });
});
