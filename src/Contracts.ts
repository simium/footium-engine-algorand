import * as address from "algosdk/src/encoding/address";
import * as algosdk from "algosdk";
import * as msgpack from "@msgpack/msgpack";
import * as R from "ramda";

export const toVaruint = number => {
    const f = (acc, number) => {
        const thisValue = number & 127;

        const newNumber = Math.floor(number / 128);

        if (newNumber === 0) {
            return R.append(thisValue, acc);
        }

        return f(R.append(thisValue | 128, acc), newNumber);
    };

    return f([], number);
};

const replaceSubArray = (subarray, replacement) => array => {
    const arrayString = R.toString(array);
    const subarrayString = R.join(", ", subarray);
    const replacementString = R.join(", ", replacement);

    const replacedString = R.replace(subarrayString, replacementString, arrayString);

    return JSON.parse(replacedString);
};

// the contract address that accepts any transaction
export const acceptAll = params => {
    const contract = [2, 32, 1, 1, 34];

    return Buffer.from(contract).toString("base64");
};

export const authClub = params => {
    const { gameState } = params;

    const assetIndex = params.assetIndex
        || gameState._algorand.clubs[params.clubId].assetIndex;

    const contract = R.pipe(
        replaceSubArray(toVaruint(123123), toVaruint(assetIndex))
    )([
        2, 32, 4, 4, 160, 141, 6, 1, 243, 193, 7, 51, 0, 16, 34, 18, 51, 0, 0,
        51, 0, 20, 18, 16, 51, 0, 1, 35, 14, 16, 51, 0, 18, 36, 18, 16, 51, 0,
        17, 37, 18, 16
    ]);

    return Buffer.from(contract).toString("base64");
};

export const getContractAddress = contract => {
    const txn = Buffer.from(msgpack.encode({
        l: Buffer.from(contract, "base64")
    })).toString("base64");

    return algosdk.logicSigFromByte(Buffer.from(txn, "base64")).address();
};

export const getClubAddress = params => {
    return getContractAddress(authClub(params));
};

export const sellPlayer = params => {
    const {
        amount, gameState, playerId
    } = params;

    const clubId = gameState.players[playerId].clubId;
    const from = getContractAddress(authClub({ gameState, clubId }));
    const assetIndex = gameState._algorand.players[playerId].assetIndex;
    const clubAssetIndex = gameState._algorand.clubs[clubId].assetIndex;

    const contract = R.pipe(
        replaceSubArray(R.repeat(0, 32), address.decode(from).publicKey),
        replaceSubArray(toVaruint(4294967296), toVaruint(amount)),
        replaceSubArray(toVaruint(123401), toVaruint(assetIndex)),
        replaceSubArray(toVaruint(123402), toVaruint(clubAssetIndex))
    )([
        2, 32, 10, 5, 4, 232, 7, 1, 128, 128, 128, 128, 16, 137, 196, 7, 0, 20,
        138, 196, 7, 192, 132, 61, 38, 2, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 45,
        79, 190, 52, 204, 225, 207, 166, 9, 67, 244, 27, 60, 177, 6, 248, 12,
        33, 28, 58, 206, 19, 104, 98, 244, 55, 234, 227, 35, 93, 211, 230, 50,
        4, 34, 18, 51, 0, 16, 35, 18, 16, 51, 0, 0, 51, 0, 20, 18, 16, 51, 0,
        1, 36, 14, 16, 51, 0, 18, 37, 18, 16, 51, 1, 16, 37, 18, 16, 51, 1, 7,
        40, 18, 16, 51, 1, 1, 36, 14, 16, 51, 1, 8, 33, 4, 18, 16, 51, 2, 16,
        35, 18, 16, 51, 2, 0, 51, 1, 0, 18, 16, 51, 2, 20, 51, 1, 0, 18, 16,
        51, 2, 1, 36, 14, 16, 51, 2, 17, 33, 5, 18, 16, 51, 2, 18, 33, 6, 18,
        16, 51, 3, 20, 51, 1, 0, 18, 16, 51, 3, 16, 35, 18, 16, 51, 3, 1, 36,
        14, 16, 51, 3, 17, 51, 2, 17, 18, 16, 51, 3, 18, 37, 18, 16, 51, 4, 16,
        37, 18, 16, 51, 4, 0, 51, 1, 0, 18, 16, 51, 4, 7, 41, 18, 16, 51, 4, 8,
        51, 1, 8, 33, 7, 10, 18, 16, 51, 4, 1, 36, 14, 16, 50, 4, 34, 18, 51,
        0, 16, 35, 18, 16, 51, 0, 0, 51, 0, 20, 18, 16, 51, 0, 1, 36, 14, 16,
        51, 0, 18, 37, 18, 16, 51, 0, 17, 33, 8, 18, 16, 51, 1, 16, 37, 18, 16,
        51, 1, 0, 40, 18, 16, 51, 1, 1, 36, 14, 16, 51, 1, 8, 33, 9, 18, 16,
        51, 2, 16, 35, 18, 16, 51, 2, 0, 51, 1, 7, 18, 16, 51, 2, 20, 51, 1, 7,
        18, 16, 51, 2, 1, 36, 14, 16, 51, 2, 17, 33, 5, 18, 16, 51, 2, 18, 33,
        6, 18, 16, 51, 3, 16, 35, 18, 16, 51, 3, 0, 51, 1, 0, 18, 16, 51, 3,
        20, 51, 1, 7, 18, 16, 51, 3, 1, 36, 14, 16, 51, 3, 17, 51, 2, 17, 18,
        16, 51, 3, 18, 37, 18, 16, 51, 4, 16, 37, 18, 16, 51, 4, 0, 51, 1, 0,
        18, 16, 51, 4, 7, 41, 18, 16, 51, 4, 8, 33, 6, 18, 16, 51, 4, 1, 36,
        14, 16, 17
    ]);

    return Buffer.from(contract).toString("base64");
};

export const playerBid = params => {
    const {
        amount, gameState, playerId, playerAssetIndex, clubId, clubAssetIndex
    } = params;

    const from = getContractAddress(authClub({
        gameState,
        clubId,
        assetIndex: clubAssetIndex
    }));
    const thisPlayerAssetIndex = playerAssetIndex ||
        gameState._algorand.players[playerId].assetIndex;
    const thisClubAssetIndex = clubAssetIndex ||
        gameState._algorand.clubs[clubId].assetIndex;

    const contract = R.pipe(
        replaceSubArray(R.repeat(0, 32), address.decode(from).publicKey),
        replaceSubArray(toVaruint(123401), toVaruint(thisClubAssetIndex)),
        replaceSubArray(toVaruint(123402), toVaruint(thisPlayerAssetIndex)),
        replaceSubArray(toVaruint(123403), toVaruint(amount))
    )([
        2, 32, 8, 3, 4, 144, 78, 1, 137, 196, 7, 0, 138, 196, 7, 139, 196, 7,
        38, 2, 32, 45, 79, 190, 52, 204, 225, 207, 166, 9, 67, 244, 27, 60,
        177, 6, 248, 12, 33, 28, 58, 206, 19, 104, 98, 244, 55, 234, 227, 35,
        93, 211, 230, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 4, 34, 18, 64, 0, 8, 50,
        4, 35, 18, 64, 0, 81, 0, 51, 0, 16, 35, 18, 51, 0, 0, 51, 0, 20, 18,
        16, 51, 0, 1, 36, 14, 16, 51, 0, 18, 37, 18, 16, 51, 0, 17, 33, 4, 18,
        16, 51, 1, 16, 37, 18, 16, 51, 1, 1, 36, 14, 16, 51, 2, 16, 37, 18, 16,
        51, 2, 0, 51, 1, 9, 18, 16, 51, 2, 7, 40, 18, 16, 51, 2, 8, 33, 5, 18,
        16, 51, 2, 1, 36, 14, 16, 66, 0, 124, 51, 0, 16, 35, 18, 51, 0, 0, 51,
        0, 20, 18, 16, 51, 0, 1, 36, 14, 16, 51, 0, 18, 37, 18, 16, 51, 1, 16,
        35, 18, 16, 51, 1, 20, 41, 18, 16, 51, 1, 1, 36, 14, 16, 51, 1, 17, 33,
        6, 18, 16, 51, 1, 18, 37, 18, 16, 51, 2, 16, 37, 18, 16, 51, 2, 7, 51,
        1, 0, 18, 16, 51, 2, 9, 51, 1, 20, 18, 16, 51, 2, 1, 36, 14, 16, 51, 2,
        8, 33, 7, 15, 16, 51, 3, 16, 37, 18, 16, 51, 3, 0, 51, 1, 0, 18, 16,
        51, 3, 7, 40, 18, 16, 51, 3, 8, 33, 5, 18, 16, 51, 3, 1, 36, 14, 16
    ]);

    return Buffer.from(contract).toString("base64");
};

export const getPlayerBidAddress = params => {
    return getContractAddress(playerBid(params));
};
