import { Actions } from "@simium/footium-engine";
import * as R from "ramda";

export const reduce = (state: any, transaction: any) => {
    const { action } = transaction;

    console.log(action);
    switch (action.type) {
    case "CLUB_TRANSFER":
        return R.compose(
            R.set(
                R.lensPath(["_algorand", "clubs", action.clubId, "assetIndex"]),
                action.clubAssetIndex
            ),
            R.set(
                R.lensPath(["_algorand", "clubs", action.clubId, "address"]),
                action.clubAddress
            )
        )(state);
    
    case "PLAYER_TRANSFER":
        return state;
    
    case "PLAYER_MINTED":
        return R.compose(
            R.set(
                R.lensPath(["_algorand", "players", action.playerId, "assetIndex"]),
                transaction.group[1]["created-asset-index"]
            )
        )(state);
    
    case "PLAYER_BID_PLACED":
        return state;

    case "PLAYER_BID_ACCEPTED":
        return state;

    case "PLAYER_BID_WITHDRAWN":
        return state;

    case "PLAYER_OFFERED":
        return state;

    case "PLAYER_OFFER_WITHDRAWN":
        return state;

    case "PLAYER_BOUGHT":
        return state;

    case "CLUB_BID_PLACED":
        return state;

    case "CLUB_BID_ACCEPTED":
        return state;

    case "CLUB_OFFERED":
        return state;

    case "CLUB_BOUGHT":
        return state;

    case "CLUB_ACCOUNT_WITHDRAWN":
        return state;

    case "ACCOUNT_WITHDRAWN":
        return state;

    case "DYNAMIC.MATCH_RESULT":
        return state;

    case "ERROR":
        console.error("Unknown action");
        console.error(action);

    default:
        return state;
    };
};
