import { Player } from "@simium/footium-engine";

const clubTransfer = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action } = transaction;
    const { sender } = transaction.transaction;

    if (!action.clubId || !action.addressFrom || !action.addressTo) {
        return false;
    }

    const isTakeOwnership = action.addressTo === sender
        && action.addressFrom === nullAddress;

    if (isTakeOwnership) {
        const clubIsntOwned = state.clubs[action.clubId].owner === null;

        return clubIsntOwned && action.clubAssetIndex;
    }

    const senderIsSender = action.addressFrom === sender;
    const senderOwnsClub = state.clubs[action.clubId].owner === sender;

    return senderIsSender && senderOwnsClub;
};

const playerMinted = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action } = transaction;
    const { sender } = transaction.transaction;
    const { group } = transaction;

    const isPlayerId = Player.checkId(action.playerId);

    if (!isPlayerId) {
        return false;
    }

    const { clubId } = Player.fromId(action.playerId);

    const { assetIndex } = state._algorand.clubs[clubId];

    const correctClub = assetIndex === transaction.group[0]["asset-transfer-transaction"]["asset-id"];
    const playerIdIsFree = state.players[action.playerId] === undefined;

    return correctClub && playerIdIsFree;
};

const playerOffered = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action } = transaction;

    const clubId = state.players[action.playerId].clubId;
    const isPlayerId = Player.checkId(action.playerId);

    return state.clubs[clubId] && action.playerId && action.minValue && isPlayerId;
}

const playerBought = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action } = transaction;
    const isPlayerId = Player.checkId(action.playerId);

    return state.clubs[action.clubId] && state.players[action.playerId] && isPlayerId;
};

const playerBidPlaced = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action } = transaction;

    // correct contract
    const optInTransaction = transaction.group[1];
    const transferTransaction = transaction.group[2];

    const realPlayerAssetIndex =
        state._algorand.players[action.playerId].assetIndex;
    const receiver = transferTransaction["payment-transaction"].receiver;
    const playerAssetIndex =
        optInTransaction["asset-transfer-transaction"]["asset-id"];

    const isCorrectContract = receiver === action.escrowAddress
        && playerAssetIndex === realPlayerAssetIndex;

    // transfer correct amount
    const value = transferTransaction["payment-transaction"].amount;

    const isCorrectAmount = value >= action.value;
    const isPlayerId = Player.checkId(action.playerId);

    return isCorrectContract && isCorrectAmount && isPlayerId;
};

const playerBidWithdrawn = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action, group } = transaction;

    const { sender } = group[1];

    const isPlayerId = Player.checkId(action.playerId);

    return sender === action.escrowAddress && isPlayerId;
};

const playerBidAccepted = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action, group } = transaction;

    const { sender } = group[2];

    const isPlayerId = Player.checkId(action.playerId);

    return sender === action.escrowAddress && isPlayerId;
};

const _isValid = (state: any, transaction: any, nullAddress: any): boolean => {
    const { action } = transaction;

    switch (action.type) {
    case "CLUB_TRANSFER":
        return clubTransfer(state, transaction, nullAddress);
    
    case "PLAYER_TRANSFER":
        return true;
    
    case "PLAYER_MINTED":
        return playerMinted(state, transaction, nullAddress);
   
    case "PLAYER_BID_PLACED":
        return playerBidPlaced(state, transaction, nullAddress);

    case "PLAYER_BID_ACCEPTED":
        return playerBidAccepted(state, transaction, nullAddress);

    case "PLAYER_BID_WITHDRAWN":
        return playerBidWithdrawn(state, transaction, nullAddress);

    case "PLAYER_OFFERED":
        return playerOffered(state, transaction, nullAddress);

    case "PLAYER_OFFER_WITHDRAWN":
        return true;

    case "PLAYER_BOUGHT":
        return playerBought(state, transaction, nullAddress);

    case "CLUB_BID_PLACED":
        return true;

    case "CLUB_BID_ACCEPTED":
        return true;

    case "CLUB_OFFERED":
        return true;

    case "CLUB_BOUGHT":
        return true;

    case "CLUB_ACCOUNT_WITHDRAWN":
        return true;

    case "ACCOUNT_WITHDRAWN":
        return true;

    case "ERROR":
        console.error("Unknown action");
        console.error(action);

    default:
        return state;
    };
};

export const isValid = (state: any, transaction: any, nullAddress: any): boolean => {
    try {
        return _isValid(state, transaction, nullAddress);
    } catch(error) {
        return false;
    }
};
