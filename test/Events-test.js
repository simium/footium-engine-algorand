const { expect } = require("chai");

const Events = require("../src/Events");

describe("Events", () => {
    const seed = "footium";

    describe("decode", () => {
        context("given offer player for sale", () => {
            const event = seed + ",sellp,42,1000";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "PLAYER_OFFERED",
                    playerId: 42,
                    minValue: 1000
                });
            });
        });

        context("given player bought", () => {
            const event = seed + ",buyp,8,42,1000";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "PLAYER_BOUGHT",
                    playerId: 8,
                    clubId: 42,
                    value: 1000
                });
            });
        });

        context("given club transfer", () => {
            const event = seed + ",clubt,42,<afrom>,<ato>";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "CLUB_TRANSFER",
                    clubId: 42,
                    addressFrom: "<afrom>",
                    addressTo: "<ato>"
                });
            });
        });

        context("given player minted", () => {
            const event = seed + ",mintp,42,4,2,1997";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "PLAYER_MINTED",
                    clubId: 42,
                    playerId: 4,
                    generationId: 2,
                    birthYear: 1997
                });
            });
        });

        context("given player bid", () => {
            const event = seed + ",bidp,2,42,3";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "PLAYER_BID_PLACED",
                    clubId: 42,
                    playerId: 2,
                    value: 3
                });
            });
        });

        context("given player bid withdrawal", () => {
            const event = seed + ",wbidp,2,42,3";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "PLAYER_BID_WITHDRAWN",
                    clubId: 42,
                    playerId: 2,
                    value: 3
                });
            });
        });

        context("given player bid accepted", () => {
            const event = seed + ",abidp,2,42,3";

            const result = Events.decode(seed, event);

            it("returns the decoded event", () => {
                expect(result).to.deep.equal({
                    type: "PLAYER_BID_ACCEPTED",
                    clubId: 42,
                    playerId: 2,
                    value: 3
                });
            });
        });
    });

    describe("encode", () => {
        context("given offer player for sale", () => {
            const event = {
                type: "PLAYER_OFFERED",
                playerId: 42,
                minValue: 1000
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",sellp,42,1000");
            });
        });

        context("given player bought", () => {
            const event = {
                type: "PLAYER_BOUGHT",
                playerId: 8,
                clubId: 42,
                value: 1000
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",buyp,8,42,1000");
            });
        });

        context("given club transfer", () => {
            const event = {
                type: "CLUB_TRANSFER",
                clubId: 42,
                addressFrom: "<afrom>",
                addressTo: "<ato>"
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",clubt,42,<afrom>,<ato>");
            });
        });

        context("given player minted", () => {
            const event = {
                type: "PLAYER_MINTED",
                clubId: 42,
                playerId: 4,
                generationId: 1,
                birthYear: 1998
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",mintp,42,4,1,1998");
            });
        });

        context("given a player bid", () => {
            const event = {
                type: "PLAYER_BID_PLACED",
                clubId: 42,
                playerId: 4,
                value: 3
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",bidp,4,42,3");
            });
        });

        context("given a player bid withdrawal", () => {
            const event = {
                type: "PLAYER_BID_WITHDRAWN",
                clubId: 42,
                playerId: 4,
                value: 3
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",wbidp,4,42,3");
            });
        });

        context("given a player bid accepted", () => {
            const event = {
                type: "PLAYER_BID_ACCEPTED",
                clubId: 42,
                playerId: 4,
                value: 3
            };

            const result = Events.encode(seed, event);

            it("returns the encoded event", () => {
                expect(result).to.equal(seed + ",abidp,4,42,3");
            });
        });
    });
});
