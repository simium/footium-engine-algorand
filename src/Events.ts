import * as R from "ramda";
import { Actions, Player } from "@simium/footium-engine";

import * as Contracts from "./Contracts";

export const decode = (prefix: string, event: string): Omit<Actions.Action, "timestamp"> | null => {
    const split = event.split(",");
    const [thisPrefix, type] = split;

    if (thisPrefix !== prefix) {
        return null;
    }

    switch (type) {
    case "sellp":
        return (function (): Omit<Actions.PlayerOffered, "timestamp"> {
            const [_1, _2, playerId, minValue] = split;

            return {
                type: "PLAYER_OFFERED",
                playerId: playerId,
                minValue: minValue
            };
        }());

    case "buyp":
        return (function (): Omit<Actions.PlayerBought, "timestamp"> {
            const [_1, _2, playerId, clubId, value] = split;

            return {
                type: "PLAYER_BOUGHT",
                playerId: playerId,
                clubId: parseInt(clubId),
                value: value
            };
        }());

    case "clubt":
        return (function (): Omit<Actions.ClubTransfer, "timestamp"> {
            const [_1, _2, clubId, addressFrom, addressTo, clubAssetIndex] = split;

            return {
                type: "CLUB_TRANSFER",
                clubId: parseInt(clubId),
                addressFrom: addressFrom,
                addressTo: addressTo
            };
        }());

    case "mintp":
        return (function (): Omit<Actions.PlayerMinted, "timestamp"> {
            const [_1, _2, playerId] = split;

            return {
                type: "PLAYER_MINTED",
                playerId: playerId
            };
        }());

    case "bidp":
        return (function (): Omit<Actions.PlayerBidPlaced, "timestamp"> {
            const [_1, _2, playerId, clubId, value] = split;

            return {
                type: "PLAYER_BID_PLACED",
                playerId: playerId,
                clubId: parseInt(clubId),
                value: value
            };
        }());

    case "wbidp":
        return (function (): Omit<Actions.PlayerBidWithdrawn, "timestamp"> {
            const [_1, _2, playerId, clubId, value] = split;

            return {
                type: "PLAYER_BID_WITHDRAWN",
                playerId: playerId,
                clubId: parseInt(clubId),
                value: value
            };
        }());

    case "abidp":
        return (function (): Omit<Actions.PlayerBidAccepted, "timestamp"> {
            const [_1, _2, playerId, clubId, value] = split;

            return {
                type: "PLAYER_BID_ACCEPTED",
                playerId: playerId,
                clubId: parseInt(clubId),
                value: value
            };
        }());
    }

    return null;
};

export const encode = (seed: string, event: Actions.Action): string => {
    switch (event.type) {
    case "PLAYER_OFFERED":
        return seed + ",sellp," + event.playerId + "," + event.minValue;

    case "PLAYER_BOUGHT":
        return seed + ",buyp," + event.playerId + "," + event.clubId + ","
            + event.value;

    case "CLUB_TRANSFER":
        return seed + ",clubt," + event.clubId + "," + event.addressFrom
            + "," + event.addressTo;

    case "PLAYER_MINTED":
        return seed + ",mintp," + event.playerId;

    case "PLAYER_BID_PLACED":
        return seed + ",bidp," + event.playerId + "," + event.clubId + ","
            + event.value;

    case "PLAYER_BID_WITHDRAWN":
        return seed + ",wbidp," + event.playerId + "," + event.clubId + ","
            + event.value;

    case "PLAYER_BID_ACCEPTED":
        return seed + ",abidp," + event.playerId + "," + event.clubId + ","
            + event.value;
    }

    return "";
};

export const addMetadata = (gameState, { action, group }: { action: Actions.Action, group: Array<any> }) => {
    try {
        switch (action.type) {
        case "CLUB_TRANSFER":
            return (function () {
                const clubAssetIndex: number = group[0]["created-asset-index"];

                return R.compose<any, any, any>(
                    R.set(R.lensProp("clubAssetIndex"), clubAssetIndex),
                    R.set(R.lensProp("clubAddress"), Contracts.getClubAddress({
                        assetIndex: clubAssetIndex
                    }))
                )(action);
            }());

        case "PLAYER_BID_PLACED":
            return (function () {
                const clubAssetIndex: number =
                    group[0]["asset-transfer-transaction"]["asset-id"];
                const playerAssetIndex: number =
                    group[1]["asset-transfer-transaction"]["asset-id"];

                return R.compose<any, any, any>(
                    R.set(R.lensProp("playerAssetIndex"), playerAssetIndex),
                    R.set(
                        R.lensProp("escrowAddress"),
                        Contracts.getPlayerBidAddress({
                            playerAssetIndex: playerAssetIndex,
                            clubAssetIndex: clubAssetIndex,
                            amount: action.value
                        })
                    )
                )(action);
            }());

        case "PLAYER_BID_WITHDRAWN":
            return (function () {
                const clubAssetIndex: number =
                    group[0]["asset-transfer-transaction"]["asset-id"];
                const playerAssetIndex: number =
                    gameState._algorand.players[action.playerId].assetIndex;

                return R.compose<any, any>(
                    R.set(
                        R.lensProp("escrowAddress"),
                        Contracts.getPlayerBidAddress({
                            playerAssetIndex: playerAssetIndex,
                            clubAssetIndex: clubAssetIndex,
                            amount: action.value
                        })
                    )
                )(action);
            }());

        case "PLAYER_BID_ACCEPTED":
            return (function () {
                const clubAssetIndex: number =
                    gameState._algorand.clubs[action.clubId].assetIndex;
                const playerAssetIndex: number =
                    gameState._algorand.players[action.playerId].assetIndex;

                return R.compose<any, any>(
                    R.set(
                        R.lensProp("escrowAddress"),
                        Contracts.getPlayerBidAddress({
                            playerAssetIndex: playerAssetIndex,
                            clubAssetIndex: clubAssetIndex,
                            amount: action.value
                        })
                    )
                )(action);
            }());
        }
    } catch (error) {
        return action;
    }

    return action;
};
