# footium-engine-algorand

This repository will contain Algorand specific code for the game engine. This is mainly to keep the front-end and the API in sync and using the same code. Before this repository there was duplicated code across the API and front-end.